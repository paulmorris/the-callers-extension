A browser extension that provides an enhanced user interface for the Caller's Box website, for contra dance callers. The main feature is bookmarking dances.
