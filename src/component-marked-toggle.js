/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals el dbDiscardDanceViaBoxId dbMarkDanceViaBoxId */

/* exported MarkedToggle */

"use strict";

async function handleMarkedToggleClick(
  boxId,
  isMarked,
  setIsMarked,
  setIsPending,
  event
) {
  event.preventDefault();

  if (!isMarked) {
    setIsPending(true);
  }

  const fn = isMarked ? dbDiscardDanceViaBoxId : dbMarkDanceViaBoxId;

  try {
    await fn(boxId);
    setIsPending(false);
    setIsMarked(!isMarked);
  } catch (e) {
    console.error(e);
  }
}

const MarkedToggle = ({
  boxId,
  isMarked,
  setIsMarked,
  isPending,
  setIsPending
}) =>
  el("a", {
    href: "#",
    onClick: handleMarkedToggleClick.bind(
      undefined,
      boxId,
      isMarked,
      setIsMarked,
      setIsPending
    ),
    className: isPending
      ? "pendingMarkedToggle"
      : isMarked
      ? "markedToggle"
      : "notMarkedToggle",
    title: isMarked
      ? "Click to un-bookmark this dance"
      : "Click to bookmark this dance"
  });
