/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals React */

/* exported el u danceUrl boxIdFromUrl compareTitleFields */

"use strict";

const el = React.createElement;
const u = undefined;
const danceUrl =
  "http://www.ibiblio.org/contradance/thecallersbox/dance.php?id=";

const boxIdFromUrl = url => {
  const matched = url.match(/\?id=(\d*)/);
  if (matched) {
    return Number(matched[1]);
  } else {
    console.warn("Was not able to get ID from URL.");
    return undefined;
  }
};

const compareTitleFields = (a, b) => {
  // Ignore case, and if names are the same return 0.
  var aTitle = a.title.toUpperCase();
  var bTitle = b.title.toUpperCase();
  return aTitle < bTitle ? -1 : aTitle > bTitle ? 1 : 0;
};
