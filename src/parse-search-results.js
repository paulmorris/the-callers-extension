/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals boxIdFromUrl */

/* exported getTableData getSearchResultsMetaData */

"use strict";

// search results table parsing
const cellsFromRow = row => row.getElementsByTagName("td");
const hasFiguresFromCells = cells => cells[0].children.length !== 0;
const hasLinkFromCells = cells => cells[1].children.length !== 0;
const hasVideoFromCells = cells => cells[2].children.length !== 0;
const titleAnchorFromCells = cells => cells[3].firstElementChild;
const authorFromCells = cells => cells[4].innerText;
const formationFromCells = cells => cells[5].innerText;

const deprecatedFromCells = cells => {
  const span = cells[2].getElementsByTagName("span")[0];
  return span && span.innerText === "(deprecated)" ? true : false;
};

const idAndTitleFromCells = cells => {
  const anchor = titleAnchorFromCells(cells);
  const boxId = boxIdFromUrl(anchor.href);
  const title = anchor.innerText;
  return [boxId, title];
};

// The parameter is a DOM node.
function getTableData(table) {
  let dances = [];
  const rows = table.getElementsByTagName("tr");
  for (let row of rows) {
    const cells = cellsFromRow(row);
    const [boxId, title] = idAndTitleFromCells(cells);
    dances.push({
      boxId,
      hasFigures: hasFiguresFromCells(cells),
      hasLink: hasLinkFromCells(cells),
      hasVideo: hasVideoFromCells(cells),
      title,
      author: authorFromCells(cells),
      formation: formationFromCells(cells),
      deprecated: deprecatedFromCells(cells)
    });
  }
  return dances;
}

function getSearchResultsMetaData(body) {
  const headingElement = body.getElementsByTagName("h2")[2];
  const databaseUpdatedElement = headingElement.nextElementSibling;
  const siteCodeUpdatedElement = databaseUpdatedElement.nextElementSibling;
  const criteriaElement =
    siteCodeUpdatedElement.nextElementSibling.nextElementSibling;
  const matchesElement = criteriaElement.nextElementSibling;

  const dateRegex = /\d\d\d\d-\d\d-\d\d/g;

  return {
    databaseUpdated: databaseUpdatedElement.textContent.match(dateRegex)[0],
    siteCodeUpdated: siteCodeUpdatedElement.textContent.match(dateRegex)[0],
    criteria: criteriaElement.textContent.trim(),
    matches: matchesElement.textContent.trim()
  };
}
