/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals React ReactDOM el DancesTable SearchForm
  dbGetMarkedDances compareTitleFields */

"use strict";

const App = props => {
  const [danceHtmlToView, setDanceHtmlToView] = React.useState("");

  return el("div", { className: "gridContainer" }, [
    el(SearchForm, { key: "searchForm" }),
    el(
      "div",
      { className: "middleColumn", key: "middleColumn" },
      props.loadingSearchResults
        ? el("div", { className: "danceListInfo" }, "Searching...")
        : props.searchResultsMetaData
        ? [
            el(
              "div",
              { className: "danceListInfo", key: "danceListInfo" },
              props.searchResultsMetaData.matches
            ),
            el(DancesTable, {
              ...props,
              setDanceHtmlToView,
              key: "dancesTable"
            }),
            el(
              "div",
              { className: "danceListInfo", key: "danceListInfoOne" },
              "Search criteria: " + props.searchResultsMetaData.criteria
            ),
            el(
              "div",
              { className: "danceListInfo", key: "danceListInfoTwo" },
              "Database updated: " +
                props.searchResultsMetaData.databaseUpdated +
                " Site Code Updated: " +
                props.searchResultsMetaData.siteCodeUpdated
            )
          ]
        : [
            el(
              "div",
              { className: "danceListInfo", key: "danceListInfo" },
              "All Bookmarked Dances"
            ),
            el(DancesTable, {
              ...props,
              setDanceHtmlToView,
              key: "dancesTable"
            })
          ]
    ),
    el("div", {
      id: "dance",
      className: "rightColumn",
      key: "rightColumn",
      /* HTML will be sanitized so it won't actually be dangerous. */
      dangerouslySetInnerHTML: { __html: danceHtmlToView }
    })
  ]);
};

async function showOnlyMarkedDances() {
  try {
    const dances = await dbGetMarkedDances();

    dances.sort(compareTitleFields);
    const container = document.getElementById("app");

    ReactDOM.render(el(App, { dances, onlyMarkedDances: true }), container);
  } catch (e) {
    console.error(e);
  }
}

window.addEventListener("load", showOnlyMarkedDances);
