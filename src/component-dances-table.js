/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals React el u danceUrl dbGetDanceViaBoxId dbCacheDanceViaBoxId
  MarkedToggle */

/* exported DancesTable */

"use strict";

const formationAbbreviations = {
  "Duple Minor - Improper": "DMI",
  "Duple Minor - Becket": "DMB",
  "Duple Minor - Proper": "DMP",
  "Duple Minor - Indecent": "DMIN",
  "Duple Minor - Reverse progression improper": "DMRPI",
  "Duple Minor - Progressed improper": "DMPI",
  "Duple Minor - Cross": "DMC",
  "Duple Minor - Other": "DMO",
  "Triple Minor": "TM",
  "Three Facing Three": "3F3",
  "Four Facing Four": "4F4",
  Triplet: "T",
  "Longways: 5+ couples": "L5+",
  "Other Longways": "OL",
  "Circle Mixer": "CM",
  "Circle of Threesomes": "C3",
  "Sicilian Circle": "SC",
  "Scatter Mixer": "SM",
  "Grid Contra": "GC",
  "Grid Square": "GS",
  Zia: "Z",
  other: "O"
};

const getFormationAbbreviation = formation =>
  formationAbbreviations[formation] || "?";

const loadingDanceHtml = '<p class="loadingDanceMessage">Loading...</p>';

async function openDanceView(boxId, setDanceHtmlToView) {
  try {
    const cachedDance = await dbGetDanceViaBoxId(boxId);
    const dance =
      cachedDance ||
      (setDanceHtmlToView(loadingDanceHtml), await dbCacheDanceViaBoxId(boxId));

    setDanceHtmlToView(dance.htmlSource);
  } catch (e) {
    console.error(e);
  }
}

const TableRow = props => {
  // This state is here to allow styling the whole row based on it.
  const [isMarked, setIsMarked] = React.useState(props.isMarked);
  const [isPending, setIsPending] = React.useState(false);

  const dance = props.dance;

  return el(
    "tr",
    {
      className: isMarked ? "markedRow" : "notMarkedRow"
    },
    [
      el(
        "td",
        { className: "resultsTableCell", key: "isMarked" },
        el(MarkedToggle, {
          boxId: dance.boxId,
          isMarked,
          setIsMarked,
          isPending,
          setIsPending
        })
      ),
      el(
        "td",
        { className: "resultsTableCell", key: "hasFigures" },
        dance.hasFigures &&
          el("span", { className: "badge", title: "Has figures" }, "Ⓕ")
      ),
      el(
        "td",
        { className: "resultsTableCell", key: "hasLink" },
        dance.hasLink &&
          el(
            "span",
            { className: "badge", title: "Has link to source for figures" },
            "Ⓛ"
          )
      ),
      el(
        "td",
        { className: "resultsTableCell", key: "hasVideo" },
        dance.hasVideo &&
          el("span", { className: "badge", title: "Has link to video(s)" }, "Ⓥ")
      ),
      el("td", { className: "resultsTableCell", key: "title" }, [
        el(
          "a",
          {
            className: "linkText",
            key: "titleAnchor",
            onClick: openDanceView.bind(
              undefined,
              dance.boxId,
              props.setDanceHtmlToView
            )
          },
          dance.title
        ),
        dance.deprecated &&
          el(
            "span",
            {
              className: "deprecated",
              title: "The author of this dance advises that you not use it.",
              key: "deprecated"
            },
            "(deprecated)"
          )
      ]),
      el(
        "td",
        { className: "resultsTableCell author", key: "author" },
        dance.author
      ),
      el(
        "td",
        {
          className: "resultsTableCell formation",
          key: "formation",
          title: dance.formation
        },
        getFormationAbbreviation(dance.formation)
      ),
      el(
        "td",
        { className: "tcb resultsTableCell", key: "tcb" },
        el(
          "a",
          {
            href: danceUrl + dance.boxId,
            target: "_blank",
            title: "Open the Caller's Box page",
            key: "title"
          },
          "CB"
        )
      )
    ]
  );
};

const DancesTable = props =>
  el(
    "table",
    { className: "searchResults" },
    el(
      "tbody",
      u,
      props.dances.map(dance => {
        const isMarked =
          props.onlyMarkedDances || props.markedBoxIds.includes(dance.boxId);

        return el(TableRow, {
          dance,
          isMarked,
          setDanceHtmlToView: props.setDanceHtmlToView,
          key: String(dance.boxId)
        });
      })
    )
  );
