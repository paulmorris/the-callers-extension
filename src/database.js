/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals Dexie danceUrl DOMPurify parseDancePage */

/* exported dbHasDanceWithBoxId dbGetAllBoxIds dbGetMarkedBoxIds
  dbGetAllDances dbGetMarkedDances dbMarkDanceViaBoxId
  dbCacheDanceViaBoxId dbDeleteDance dbDeleteDanceViaBoxId
  dbDiscardDanceViaBoxId */

"use strict";

const db = new Dexie("CallersExtensionDatabase");

// boxId is the Caller's Box id.
// Unindexed fields: hasFigures, hasLink, hasVideo, deprecated
// storageStatus can be: "cached", "marked", "discarded"
// "discarded" means it was marked but is now unmarked.
db.version(1).stores({
  dances: "++id, boxId, title, author, formation, storageStatus"
});

const dbHasDanceWithBoxId = boxId =>
  db.dances
    .where("boxId")
    .equals(boxId)
    .count()
    .then(count => count > 0);

const dbGetAllBoxIds = () =>
  db.dances
    .where("boxId")
    .notEqual(-1)
    .keys();

const dbGetMarkedBoxIds = async () => {
  const boxIds = await db.dances
    .where("storageStatus")
    .equals("marked")
    .toArray(dances => dances.map(d => d.boxId));
  return boxIds || Promise.resolve([]);
};

const dbGetAllDances = () => db.dances.toArray();

const dbGetMarkedDances = async () => {
  const dances = await db.dances
    .where("storageStatus")
    .equals("marked")
    .toArray();
  return dances || Promise.resolve([]);
};

const fetchDanceData = async boxId => {
  const response = await fetch(danceUrl + boxId);
  const rawHtml = await response.text();
  const html = DOMPurify.sanitize(rawHtml);

  const parser = new DOMParser();
  const doc = parser.parseFromString(html, "text/html");
  const body = doc.body;

  // Remove the first <p> that has "This page is part of..."
  body.querySelector("p").remove();

  const dance = parseDancePage(body, boxId);

  dance.dateMarked = new Date().toJSON();

  return dance;
};

const dbAddDance = async (dance, storageStatus) => {
  dance.storageStatus = storageStatus;
  const id = await db.dances.add(dance);
  dance.id = id;
  return dance;
};

// Tempting to use a transaction, but they can't include
// 3rd party async calls, like to fetch the dance page.
//
const dbMarkDanceViaBoxId = async boxId => {
  const storageStatus = "marked";
  const dance = await dbGetDanceViaBoxId(boxId);

  if (dance) {
    await db.dances.update(dance.id, {
      storageStatus: storageStatus
    });
    dance.storageStatus = storageStatus;
    return dance;
  } else {
    const fetchedDance = await fetchDanceData(boxId);
    const addedDance = await dbAddDance(fetchedDance, storageStatus);
    return addedDance;
  }
};

const dbCacheDanceViaBoxId = async boxId => {
  const fetchedDance = await fetchDanceData(boxId);
  const addedDance = await dbAddDance(fetchedDance, "cached");
  return addedDance;
};

const dbGetDanceViaBoxId = boxId => db.dances.get({ boxId });

const dbDeleteDance = dance =>
  db.transaction("rw", db.dances, async () => {
    const id =
      dance.id ||
      (await dbGetDanceViaBoxId(dance.boxId).then(dance => dance.id));
    return db.dances.delete(id);
  });

const dbDeleteDanceViaBoxId = boxId =>
  db.transaction("rw", db.dances, async () => {
    const dance = await dbGetDanceViaBoxId(boxId);
    return db.dances.delete(dance.id);
  });

const dbDiscardDanceViaBoxId = boxId =>
  db.transaction("rw", db.dances, async () => {
    const dance = await dbGetDanceViaBoxId(boxId);
    return await db.dances.update(dance.id, { storageStatus: "discarded" });
  });
