/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* exported parseDancePage */

"use strict";

function getDanceDataFromPs(rootElement) {
  const pElements = rootElement.getElementsByTagName("p");
  let hasFigures = true,
    author,
    deprecated = false;

  for (let p of pElements) {
    if (p.innerText.match(/Permission: search/)) {
      hasFigures = false;
    }
    if (p.innerText.match(/^by /)) {
      author = p.innerText.slice(3);
    }
    if (p.innerText.match(/^Status: deprecated/)) {
      deprecated = true;
    }
  }
  return [hasFigures, author, deprecated];
}

function getDanceDataFromTds(rootElement) {
  const tdElements = rootElement.getElementsByTagName("td");
  let hasLink = false,
    hasVideo = false,
    formation;

  for (let td of tdElements) {
    if (
      td.innerText.match(/Appearances:/) &&
      td.nextElementSibling.getElementsByTagName("a").length > 0
    ) {
      hasLink = true;
    }
    if (td.innerText.match(/FormationBase:/)) {
      formation = td.nextElementSibling.innerText;
    }
    if (
      td.innerText.match(/Videos:/) &&
      td.nextElementSibling.getElementsByTagName("a").length > 0
    ) {
      hasVideo = true;
    }
  }
  return [hasLink, hasVideo, formation];
}

const makeGlossaryHrefsAbsolute = html =>
  html.replace(
    /href="Glossary\.htm/gi,
    'href="http://www.ibiblio.org/contradance/thecallersbox/Glossary.htm'
  );

const makeLinkTargetsBlank = html =>
  html.replace(/<a /gi, '<a target="_blank" ');

const modifyHtml = html =>
  makeLinkTargetsBlank(makeGlossaryHrefsAbsolute(html));

function parseDancePage(rootElement, boxId) {
  const h1 = rootElement.getElementsByTagName("h1")[0];
  const title = h1.innerText;
  const [hasFigures, author, deprecated] = getDanceDataFromPs(rootElement);
  const [hasLink, hasVideo, formation] = getDanceDataFromTds(rootElement);
  const htmlSource = modifyHtml(rootElement.innerHTML);
  return {
    boxId,
    hasFigures,
    hasLink,
    hasVideo,
    title,
    author,
    formation,
    deprecated,
    htmlSource
  };
}
