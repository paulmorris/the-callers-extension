/*
Copyright 2019, Paul Morris

This file is part of The Caller's Extension.

The Caller's Extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The Caller's Extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with The Caller's Extension.  If not, see <https://www.gnu.org/licenses/>.
*/

/* globals React ReactDOM el App DOMPurify getSearchResultsMetaData
  getTableData dbGetMarkedBoxIds showOnlyMarkedDances */

/* exported SearchForm */

"use strict";

function hoistMarkedDances(markedBoxIds, dances) {
  let isMarked = [];
  let others = [];
  dances.forEach(dance =>
    markedBoxIds.includes(dance.boxId)
      ? isMarked.push(dance)
      : others.push(dance)
  );
  return [...isMarked, ...others];
}

/**
 * Returns a string using Windows ("%0D%0A") newlines, rather
 * than Linux/Unix/Mac ("%0A") newlines.
 */
const windowsNewlines = str =>
  str.match(/%0D%0A/g) ? str : str.replace(/%0A/g, "%0D%0A");

function getUrlFromForm(formElement) {
  const rawParams = new URLSearchParams(new FormData(formElement)).toString();
  const params = windowsNewlines(rawParams);
  const baseUrl = "http://www.ibiblio.org/contradance/thecallersbox/index.php";
  return baseUrl + "?" + params;
}

async function doCallersBoxSearch(searchUrl) {
  try {
    const container = document.getElementById("app");

    ReactDOM.render(
      el(App, {
        dances: [],
        markedBoxIds: [],
        searchResultsMetaData: {},
        searchUrl: searchUrl,
        loadingSearchResults: true
      }),
      container
    );

    const response = await fetch(searchUrl);
    const rawHtml = await response.text();
    const html = DOMPurify.sanitize(rawHtml);

    const parser = new DOMParser();
    const doc = parser.parseFromString(html, "text/html");
    const body = doc.body;

    const searchResultsMetaData = getSearchResultsMetaData(body);

    const table = body.getElementsByTagName("table")[0];
    const dances = table ? getTableData(table) : [];

    const markedBoxIds = await dbGetMarkedBoxIds();

    const reorderedDances = hoistMarkedDances(markedBoxIds, dances);

    ReactDOM.render(
      el(App, {
        dances: reorderedDances,
        markedBoxIds,
        searchResultsMetaData,
        searchUrl,
        loadingSearchResults: false
      }),
      container
    );
  } catch (e) {
    console.error(e);
  }
}

function handleSearchFormSubmit(input, event) {
  event.preventDefault();
  const formElement = input.current;
  const searchUrl = getUrlFromForm(formElement) + "&show_all";
  doCallersBoxSearch(searchUrl);
}

// Uncontrolled form component, uses a ref to access form DOM node.
const SearchForm = () => {
  const [input] = React.useState(React.createRef());

  return el(
    "div",
    {
      className: "leftColumn",
      key: "searchFormWrapper"
    },
    [
      el(
        "div",
        { className: "appTitle", key: "appTitle" },
        "The Caller's Extension"
      ),
      el("div", { className: "appSubtitle", key: "appSubtitle" }, [
        el("span", { key: "builtOn" }, "Built on top of "),
        el(
          "a",
          {
            href: "http://www.ibiblio.org/contradance/thecallersbox/",
            target: "_blank",
            key: "theCallersBox",
            className: "callersBoxLink"
          },
          "The Caller's Box"
        )
      ]),
      el(
        "button",
        {
          className: "showOnlyMarkedDancesButton",
          onClick: showOnlyMarkedDances,
          key: "showOnlyMarkedDancesButton"
        },
        "All Bookmarked Dances"
      ),
      el(
        "form",
        {
          name: "searchForm",
          id: "searchForm",
          key: "searchFormNode",
          onSubmit: handleSearchFormSubmit.bind(undefined, input),
          ref: input
        },
        [
          el(
            "div",
            { className: "searchLabelContainer", key: "titleLabel" },
            "Title contains:"
          ),
          el("input", {
            name: "title",
            placeholder: "Title",
            defaultValue: "",
            className: "searchFormTextInput",
            key: "title"
          }),
          el(
            "div",
            { className: "searchLabelContainer", key: "authorLabel" },
            "Author contains:"
          ),
          el("input", {
            name: "author",
            placeholder: "Author",
            defaultValue: "",
            className: "searchFormTextInput",
            key: "author"
          }),
          el(
            "div",
            { className: "searchLabelContainer", key: "searchLabelContainer" },
            "Formation:"
          ),
          el(
            "select",
            {
              name: "formation",
              defaultValue: "",
              className: "searchFormSelectInput",
              key: "searchFormSelectInput"
            },
            [
              el("option", { key: "noFormation" }, ""),
              el("option", { key: "dupleMinor" }, "Duple Minor"),
              el("option", { key: "improper" }, "Duple Minor - Improper"),
              el("option", { key: "becket" }, "Duple Minor - Becket"),
              el("option", { key: "proper" }, "Duple Minor - Proper"),
              el("option", { key: "indecent" }, "Duple Minor - Indecent"),
              el(
                "option",
                { key: "reverseImproper" },
                "Duple Minor - Reverse progression improper"
              ),
              el(
                "option",
                { key: "progressedImproper" },
                "Duple Minor - Progressed improper"
              ),
              el("option", { key: "cross" }, "Duple Minor - Cross"),
              el("option", { key: "dupleMinorOther" }, "Duple Minor - Other"),
              el("option", { key: "tripleMinor" }, "Triple Minor"),
              el("option", { key: "threeFacingThree" }, "Three Facing Three"),
              el("option", { key: "fourFacingFour" }, "Four Facing Four"),
              el("option", { key: "triplet" }, "Triplet"),
              el("option", { key: "longwaysFive" }, "Longways: 5+ couples"),
              el("option", { key: "longwaysOther" }, "Other Longways"),
              el("option", { key: "circleMixer" }, "Circle Mixer"),
              el("option", { key: "circleOFThree" }, "Circle of Threesomes"),
              el("option", { key: "circleSicilian" }, "Sicilian Circle"),
              el("option", { key: "scatter" }, "Scatter Mixer"),
              el("option", { key: "gridContra" }, "Grid Contra"),
              el("option", { key: "gridSquare" }, "Grid Square"),
              el("option", { key: "zia" }, "Zia"),
              el("option", { key: "other" }, "other")
            ]
          ),
          el(
            "div",
            { className: "searchLabelContainer", key: "figuresMatch" },
            "Figures match:"
          ),
          el(
            "select",
            {
              name: "pos_mode",
              defaultValue: "all_given",
              className: "searchFormSelectInput",
              key: "posMode"
            },
            [
              el(
                "option",
                { value: "any_any", key: "posAny" },
                "any lines, in any order"
              ),
              el(
                "option",
                { value: "all_any", key: "posAllAnyOrder" },
                "all lines, in any order"
              ),
              el(
                "option",
                { value: "all_given", key: "posAllGivenOrder" },
                "all lines, in given order"
              )
            ]
          ),
          el("textarea", {
            name: "pos_lines",
            className: "searchFormTextArea",
            rows: "5",
            key: "posLines"
          }),
          el(
            "div",
            { className: "searchLabelContainer", key: "figuresDoNotMatch" },
            "But figures do not match:"
          ),
          el(
            "select",
            {
              name: "neg_mode",
              defaultValue: "any_any",
              className: "searchFormSelectInput",
              key: "negMode"
            },
            [
              el(
                "option",
                { value: "any_any", key: "negAny" },
                "any lines, in any order"
              ),
              el(
                "option",
                { value: "all_any", key: "negAllAnyOrder" },
                "all lines, in any order"
              ),
              el(
                "option",
                { value: "all_given", key: "negAllGivenOrder" },
                "all lines, in given order"
              )
            ]
          ),
          el("textarea", {
            name: "neg_lines",
            className: "searchFormTextArea",
            rows: "5",
            key: "negLines"
          }),
          el(
            "div",
            { className: "searchLabelContainer", key: "searchButtonWrapper" },
            el(
              "button",
              { id: "searchButton", type: "submit" },
              "Search the Caller's Box"
            )
          ),
          el(
            "a",
            {
              href:
                "https://addons.mozilla.org" +
                "/en-US/firefox/addon/the-caller-s-extension/",
              className: "aboutLink",
              target: "_blank",
              title: "About The Caller's Extension"
            },
            "About"
          )
        ]
      )
    ]
  );
};
